package org.test.study007;

public class Main {


	public static void main(String[] args) {
		Implementor i;
		try {
			i = new Implementor(Class.forName(args[0]));
			i.fileWriter();
		} catch (ClassNotFoundException e) {
			System.err.println("Class " + args[0] + " not exist!!!");
			e.printStackTrace();
		}
	}
}
